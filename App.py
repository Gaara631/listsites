import urllib2
import re
from multiprocessing import Queue
import threading
import signal
import MySQLdb
from optparse import OptionParser
queue = Queue()
toScanQueue = Queue()
linksList = list()

def connectToDB(url,user,password,db):
    return MySQLdb.connect(url,user,password,db=db)

"""Config variables"""

parser = OptionParser()
parser.add_option("-t", "--max-threads", dest="maxthreads", default=50,
                  help="Count of maximum threads started")

parser.add_option("-s", "--start-site", dest="startsite",
                  help="Name of site to start search")

parser.add_option("-d", "--domen", dest="domen", default="",
                  help="Domen to search leave empty to search all domens")

parser.add_option("-u", "--url", dest="mysqlurl",
                  help="Mysql url")

parser.add_option("-l", "--login", dest="login",
                  help="Database login")

parser.add_option("-p", "--password", dest="password",
                  help="Database password")

parser.add_option("-r", "--table", dest="table",
                  help="Database table to add URL(Must be columns ID and URL)")

parser.add_option("-b", "--database", dest="database",
                  help="Database name")

parser.add_option("-v", "--verbose", dest="verbose", default=False,
                  help="Verbose enable",action="store_true")


options, arguments = parser.parse_args()
try:
    db = connectToDB(options.mysqlurl,options.login,options.password,options.database)
except:
    print ("Can't connect to DB. Exiting...")
    exit(1)

cursor = db.cursor()
inserted = 0
duplicated = 0


def addToDB(site):
    querycount = "SELECT COUNT(*) FROM "+options.table+";"
    try:
        cursor.execute(querycount)
        fetch = cursor.fetchone()
        count = int(fetch[0])
        count += 1

        query = "INSERT INTO " + options.table + " (ID,URL) VALUES ('"+str(count)+"','" + site.replace('\n', '') + "');"
        cursor.execute(query)
        db.commit()
        global inserted
        inserted += 1
    except MySQLdb.IntegrityError:
        global duplicated
        duplicated +=1
    except:
        print "Some DB add error"


def signal_handler(signal, frame):

    print('You pressed Ctrl+C!')

    global goOut
    if goOut:
        exit(1)
    goOut = True



    db.close()
    print ("Inserted = " + str(inserted))
    print ("Duplicated = " + str(duplicated))



class Writer(threading.Thread):
    def run(self):
        f = open('links.txt', 'w')
        while(True):
            if goOut:
                break
            if queue.qsize() > 0:
                item = queue.get()
                siteName = item[:item.find('/', 9) + 1]
                if options.verbose:
                    print (siteName)
                if (options.domen in siteName) and (siteName not in linksList):
                    f.write(siteName + "\n")
                    addToDB(siteName)
                    linksList.append(siteName)
                    if not options.verbose:
                        print (siteName)
                    toScanQueue.put(item)
            if (threading.activeCount() < int(options.maxthreads)) and (toScanQueue.qsize() > 0):
                ScanSiteThread(toScanQueue.get()).start()
        f.close()
class ScanSiteThread(threading.Thread):

    def __init__(self,url):
        super(ScanSiteThread, self).__init__()
        self.url = url
        self._stop = threading.Event()
        if options.verbose:
            print ("Thread Started" + str(threading.activeCount()))

    def __del__(self):
        if options.verbose:
            print ("Thread Ended" + str(threading.activeCount()))

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def run(self):
        try:
            website = urllib2.urlopen(self.url)
            html = website.read()
            links = re.findall('"((http)s?://.*?)"', html)

            for link in links:
                queue.put(link[0])
                if goOut:
                    break
        except urllib2.HTTPError as e:
            print (e.message)
        except Exception as e:
            print (e.message)


goOut = False
signal.signal(signal.SIGINT, signal_handler)
Writer().start()
ScanSiteThread(options.startsite).start()
signal.pause()